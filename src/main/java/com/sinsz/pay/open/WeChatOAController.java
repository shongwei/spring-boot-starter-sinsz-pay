package com.sinsz.pay.open;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 微信公众号获取授权接口
 *
 * @author chenjianbo
 */
@Controller
@RequestMapping("/{api:[a-z]+}/1.0/oa")
public class WeChatOAController {

    private final WeChatOAService weChatOAService;

    @Autowired
    public WeChatOAController(WeChatOAService weChatOAService) {
        this.weChatOAService = weChatOAService;
    }

    /**
     * 微信公众号获取授权code并重定向
     * @param api       动态请求地址
     * @return  重定向
     */
    @GetMapping("/fetch/user")
    public String fetchCode(@PathVariable String api) {
        String path = "/" + api + "/1.0/oa/fetch/openid";
        return weChatOAService.fetchCode(path);
    }

    /**
     * 微信公众号获取openid
     * @param request   获取请求参数对象
     * @param api       动态请求地址
     * @return          返回openid或重定向
     */
    @GetMapping("/fetch/openid")
    public String fetchOpenid(HttpServletRequest request, @PathVariable String api) {
        return weChatOAService.fetchOpenid(request);
    }

}